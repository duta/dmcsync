﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace EraseConsole
{
    public static class DeleteService
    {
        public static event EventHandler StatusUpdate = delegate { };
        public static event EventHandler FolderUpdate = delegate { };
        private static bool started = false;
        private static Thread _threadDelete;

        public static Status CurrentStatus = new Status();

        public static void TriggerFolderUpdate()
        {
            FolderUpdate(null, EventArgs.Empty);
        }
        public static void OnStart()
        {
            started = true;
            _threadDelete = new Thread(new ThreadStart(StartDelete));
            _threadDelete.Name = "proses delete Thread";
            _threadDelete.IsBackground = true;
            _threadDelete.Start();
            Console.WriteLine("Delete Thread Started");
        }
        public static void OnStop()
        {
            started = false;
            _threadDelete.Abort();
            CurrentStatus.Label = "default";
            CurrentStatus.Button = "success";
            CurrentStatus.ButtonText = "Activate";
            CurrentStatus.Message = "Inactive";
            StatusUpdate(null, EventArgs.Empty);
            Console.WriteLine("Delete Thread Aborted");
        }
        internal static void StartDelete()
        {
            while (started)
            {
                //Console.WriteLine("delete Folder ");
                Console.WriteLine("StartDelete " + started);
                var folders = SQLiteHandler.GetAllDelete();
                var deldays = SQLiteHandler.GetDeleteDays();
                foreach (var item in folders)
                {
                    try
                    {
                        CurrentStatus.Label = "success";
                        CurrentStatus.Button = "danger";
                        CurrentStatus.ButtonText = "Deactivate";
                        CurrentStatus.Message = "Active - Deleting files in " + item.location;
                        StatusUpdate(null, EventArgs.Empty);
                        SoddingNetworkAuth sna = null;
                        if (item.server != null) sna = new SoddingNetworkAuth(item.user, item.server, item.pass);
                        foreach (string dirs in Directory.GetDirectories(item.location))
                        {
                            DirectoryInfo dir = new DirectoryInfo(dirs);
                            if (dir.CreationTime < (DateTime.Now - new TimeSpan(deldays, 0, 10, 0)))
                            {
                                DeleteDirectory(dirs, item, true, true, deldays);
                                Console.WriteLine("delete folder: " + dir.CreationTime + "<" + (DateTime.Now - new TimeSpan(deldays, 0, 0, 0)));
                            }
                        }
                        DeleteDirectory(item.location, item, false, false, deldays);
                        if (item.server != null) sna.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + ex.StackTrace, ex);
                        //Thread.Sleep(_secInterval);
                    }
                }
                CurrentStatus.Label = "primary";
                CurrentStatus.Button = "danger";
                CurrentStatus.ButtonText = "Deactivate";
                CurrentStatus.Message = "Active - Stand by";
                StatusUpdate(null, EventArgs.Empty);
                Thread.Sleep(SQLiteHandler.GetDeleteInterval());
            }
        }

        private static void DeleteDirectory(string path, Folders item, bool recursive, bool deleteifempty, int deldays)
        {
            try
            {
                // Delete all files and sub-folders?
                if (recursive)
                {
                    // Yep... Let's do this
                    var subfolders = Directory.GetDirectories(path);
                    foreach (var s in subfolders)
                    {
                        DeleteDirectory(s, item, recursive, deleteifempty, deldays);
                    }
                }

                // Get all files of the folder
                var files = Directory.GetFiles(path);
                foreach (var f in files)
                {
                    //skip newer files
                    var file = new FileInfo(f);
                    if (file.CreationTime > (DateTime.Now - new TimeSpan(deldays, 0, 10, 0))) continue;
                    // Get the attributes of the file
                    var attr = File.GetAttributes(f);

                    // Is this file marked as 'read-only'?
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        // Yes... Remove the 'read-only' attribute, then
                        File.SetAttributes(f, attr ^ FileAttributes.ReadOnly);
                    }

                    // Delete the file
                    File.Delete(f);
                    Console.WriteLine("delete file : " + f);
                }

                // Check if directory is empty and delete if needed.
                if (!Directory.EnumerateFileSystemEntries(path).Any() && deleteifempty)
                {
                    new DirectoryInfo(path).Delete();
                    Console.WriteLine("delete folder : " + path);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace, ex);
                //Thread.Sleep(_secInterval);
            }
        }
    }

    public class Status
    {
        public string Type = "delstatus";
        public string Label = "default";
        public string Button = "success";
        public string ButtonText = "Activate";
        public string Message = "Inactive";
    }
}