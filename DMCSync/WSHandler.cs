﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace EraseConsole
{
    public class WSHandler : WebSocketBehavior
    {
        protected override void OnOpen()
        {
            DeleteService.StatusUpdate += DeleteService_StatusUpdate;
            DeleteService.FolderUpdate += DeleteService_FolderUpdate;
            OneSyncService.StatusUpdate += OneSyncService_StatusUpdate;
            OneSyncService.FolderUpdate += OneSyncService_FolderUpdate;
            TwoSyncService.StatusUpdate += TwoSyncService_StatusUpdate;
            TwoSyncService.FolderUpdate += TwoSyncService_FolderUpdate;
            Send(JsonConvert.SerializeObject(DeleteService.CurrentStatus));
            Send(JsonConvert.SerializeObject(OneSyncService.CurrentStatus));
            Send(JsonConvert.SerializeObject(TwoSyncService.CurrentStatus));
            Send(JsonConvert.SerializeObject(SQLiteHandler.GetSettings()));
            Send(JsonConvert.SerializeObject(new FolderHolder("delfolder", SQLiteHandler.GetAllDelete())));
            Send(JsonConvert.SerializeObject(new FoldersHolder("s1folder", SQLiteHandler.GetAllSyncOne())));
            Send(JsonConvert.SerializeObject(new FoldersHolder("s2folder", SQLiteHandler.GetAllSyncTwo())));
        }

        private void TwoSyncService_FolderUpdate(object sender, EventArgs e)
        {
            Send(JsonConvert.SerializeObject(new FoldersHolder("s2folder", SQLiteHandler.GetAllSyncTwo())));
        }

        private void TwoSyncService_StatusUpdate(object sender, EventArgs e)
        {
            Send(JsonConvert.SerializeObject(TwoSyncService.CurrentStatus));
        }

        private void OneSyncService_FolderUpdate(object sender, EventArgs e)
        {
            Send(JsonConvert.SerializeObject(new FoldersHolder("s1folder", SQLiteHandler.GetAllSyncOne())));
        }

        private void OneSyncService_StatusUpdate(object sender, EventArgs e)
        {
            Send(JsonConvert.SerializeObject(OneSyncService.CurrentStatus));
        }

        private void DeleteService_FolderUpdate(object sender, EventArgs e)
        {
            Send(JsonConvert.SerializeObject(SQLiteHandler.GetSettings()));
            Send(JsonConvert.SerializeObject(new FolderHolder("delfolder", SQLiteHandler.GetAllDelete())));
        }

        private void DeleteService_StatusUpdate(object sender, EventArgs e)
        {
            Send(JsonConvert.SerializeObject(DeleteService.CurrentStatus));            
        }

        protected override void OnClose(CloseEventArgs e)
        {
            DeleteService.StatusUpdate -= DeleteService_StatusUpdate;
            DeleteService.FolderUpdate -= DeleteService_FolderUpdate;
            OneSyncService.StatusUpdate -= OneSyncService_StatusUpdate;
            OneSyncService.FolderUpdate -= OneSyncService_FolderUpdate;
            TwoSyncService.StatusUpdate -= TwoSyncService_StatusUpdate;
            TwoSyncService.FolderUpdate -= TwoSyncService_FolderUpdate;
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            WSPayload wsp = JsonConvert.DeserializeObject<WSPayload>(e.Data);
            if (wsp.Type.Equals("delToggle"))
            {
                if (wsp.Values[0].Equals("Activate"))
                {
                    DeleteService.OnStart();
                }
                else
                {
                    DeleteService.OnStop();
                }
            }
            if (wsp.Type.Equals("s1Toggle"))
            {
                if (wsp.Values[0].Equals("Activate"))
                {
                    OneSyncService.OnStart();
                }
                else
                {
                    OneSyncService.OnStop();
                }
            }
            if (wsp.Type.Equals("s2Toggle"))
            {
                if (wsp.Values[0].Equals("Activate"))
                {
                    TwoSyncService.OnStart();
                }
                else
                {
                    TwoSyncService.OnStop();
                }
            }
            if (wsp.Type.Equals("settingChange"))
            {
                SQLiteHandler.ChangeSettings(int.Parse(wsp.Values[0]), int.Parse(wsp.Values[1]));
                DeleteService.TriggerFolderUpdate();
            }
            if (wsp.Type.Equals("addDel"))
            {
                SQLiteHandler.AddDelete(wsp.Values[0], wsp.Values[1], wsp.Values[2], wsp.Values[3]);
                DeleteService.TriggerFolderUpdate();
            }
            if (wsp.Type.Equals("deleteDel"))
            {
                SQLiteHandler.DeleteDelete(wsp.Values[0]);
                DeleteService.TriggerFolderUpdate();
            }
            if (wsp.Type.Equals("addS1"))
            {
                SQLiteHandler.AddSyncOne(wsp.Values[0], wsp.Values[1], wsp.Values[2], wsp.Values[3],
                    wsp.Values[4], wsp.Values[5], wsp.Values[6], wsp.Values[7]);
                OneSyncService.TriggerFolderUpdate();
            }
            if (wsp.Type.Equals("deleteS1"))
            {
                SQLiteHandler.DeleteSyncOne(wsp.Values[0], wsp.Values[1]);
                OneSyncService.TriggerFolderUpdate();
            }
            if (wsp.Type.Equals("addS2"))
            {
                SQLiteHandler.AddSyncTwo(wsp.Values[0], wsp.Values[1], wsp.Values[2], wsp.Values[3],
                    wsp.Values[4], wsp.Values[5], wsp.Values[6], wsp.Values[7]);
                TwoSyncService.TriggerFolderUpdate();
            }
            if (wsp.Type.Equals("deleteS2"))
            {
                SQLiteHandler.DeleteSyncTwo(wsp.Values[0], wsp.Values[1]);
                TwoSyncService.TriggerFolderUpdate();
            }
        }
    }

    public class WSPayload
    {
        public string Type;
        public string[] Values;
    }

    public class FoldersHolder
    {
        public string Type;
        public List<Folders[]> Folders;

        public FoldersHolder(string type, List<Folders[]> list)
        {
            this.Folders = list;
            this.Type = type;
        }
    }

    public class FolderHolder
    {
        public string Type;
        public List<Folders> Folders;

        public FolderHolder(string type, List<Folders> list)
        {
            this.Folders = list;
            this.Type = type;
        }
    }
}
