﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace EraseConsole
{
    public static class TwoSyncService
    {
        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="fromPath"/> or <paramref name="toPath"/> is <c>null</c>.</exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static string GetRelativePath(string fromPath, string toPath)
        {
            if (string.IsNullOrEmpty(fromPath))
            {
                throw new ArgumentNullException("fromPath");
            }

            if (string.IsNullOrEmpty(toPath))
            {
                throw new ArgumentNullException("toPath");
            }

            Uri fromUri = new Uri(AppendDirectorySeparatorChar(fromPath));
            Uri toUri = new Uri(AppendDirectorySeparatorChar(toPath));

            if (fromUri.Scheme != toUri.Scheme)
            {
                return toPath;
            }

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            string relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (string.Equals(toUri.Scheme, Uri.UriSchemeFile, StringComparison.OrdinalIgnoreCase))
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }
        private static string AppendDirectorySeparatorChar(string path)
        {
            // Append a slash only if the path is a directory and does not have a slash.
            if (!Path.HasExtension(path) &&
                !path.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                return path + Path.DirectorySeparatorChar;
            }

            return path;
        }

        public static event EventHandler StatusUpdate = delegate { };
        public static event EventHandler FolderUpdate = delegate { };
        private static bool started = false;
        private static Thread _threadDelete;

        public static Status CurrentStatus = new Status() { Type = "s2status" };

        public static void TriggerFolderUpdate()
        {
            FolderUpdate(null, EventArgs.Empty);
        }
        public static void OnStart()
        {
            started = true;
            _threadDelete = new Thread(new ThreadStart(StartDelete));
            _threadDelete.Name = "proses sync two Thread";
            _threadDelete.IsBackground = true;
            _threadDelete.Start();
            Console.WriteLine("Sync two Thread Started");
        }
        public static void OnStop()
        {
            started = false;
            _threadDelete.Abort();
            CurrentStatus.Label = "default";
            CurrentStatus.Button = "success";
            CurrentStatus.ButtonText = "Activate";
            CurrentStatus.Message = "Inactive";
            StatusUpdate(null, EventArgs.Empty);
            Console.WriteLine("Sync two Thread Aborted");
        }
        internal static void StartDelete()
        {
            while (started)
            {
                //Console.WriteLine("delete Folder ");
                Console.WriteLine("Start Sync two " + started);
                var folders = SQLiteHandler.GetAllSyncTwo();
                foreach (var item in folders)
                {
                    try
                    {
                        CurrentStatus.Label = "success";
                        CurrentStatus.Button = "danger";
                        CurrentStatus.ButtonText = "Deactivate";
                        CurrentStatus.Message = "Active - Syncing files in " + item[1].location;
                        StatusUpdate(null, EventArgs.Empty);
                        SoddingNetworkAuth sna = null;
                        SoddingNetworkAuth sna1 = null;
                        if (item[0].server != null) sna = new SoddingNetworkAuth(item[0].user, item[0].server, item[0].pass);
                        if (item[1].server != null) sna1 = new SoddingNetworkAuth(item[1].user, item[1].server, item[1].pass);
                        var history = SQLiteHandler.GetS2History(item[0].location, item[1].location);
                        foreach (var record in history)
                        {
                            if (!File.Exists(Path.Combine(item[0].location, record)) && File.Exists(Path.Combine(item[1].location, record)))
                            {
                                File.Delete(Path.Combine(item[1].location, record));
                                SQLiteHandler.Erase(item[0].location, item[1].location, record);
                            }
                            if (!File.Exists(Path.Combine(item[1].location, record)) && File.Exists(Path.Combine(item[0].location, record)))
                            {
                                File.Delete(Path.Combine(item[0].location, record));
                                SQLiteHandler.Erase(item[0].location, item[1].location, record);
                            }
                            if (!Directory.Exists(Path.Combine(item[0].location, record)) && Directory.Exists(Path.Combine(item[1].location, record)))
                            {
                                Directory.Delete(Path.Combine(item[1].location, record));
                                SQLiteHandler.Erase(item[0].location, item[1].location, record);
                            }
                            if (!Directory.Exists(Path.Combine(item[1].location, record)) && Directory.Exists(Path.Combine(item[0].location, record)))
                            {
                                Directory.Delete(Path.Combine(item[0].location, record));
                                SQLiteHandler.Erase(item[0].location, item[1].location, record);
                            }
                        }
                        CopyDirectory(item[0].location, item[0], item[1], item[0], item[1], true);
                        CopyDirectory(item[1].location, item[1], item[0], item[0], item[1], true);
                        if (item[0].server != null) sna.Dispose();
                        if (item[1].server != null) sna1.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + ex.StackTrace, ex);
                        //Thread.Sleep(_secInterval);
                    }
                }
                CurrentStatus.Label = "primary";
                CurrentStatus.Button = "danger";
                CurrentStatus.ButtonText = "Deactivate";
                CurrentStatus.Message = "Active - Stand by";
                StatusUpdate(null, EventArgs.Empty);
                Thread.Sleep(SQLiteHandler.GetDeleteInterval());
            }
        }
        
        private static void CopyDirectory(string path, Folders src, Folders dest, Folders trusrc, Folders trudest, bool recursive)
        {
            try
            {
                var relative = GetRelativePath(src.location, path);

                // Check if directory exists on dest and create if needed.
                if (!SQLiteHandler.Exists(trusrc.location, trudest.location, relative))
                {
                    SQLiteHandler.Record(trusrc.location, trudest.location, relative);
                    System.IO.Directory.CreateDirectory(Path.Combine(dest.location, relative));
                    Console.WriteLine("Record folder : " + relative);
                }

                // Copy all files and sub-folders?
                if (recursive)
                {
                    // Yep... Let's do this
                    var subfolders = Directory.GetDirectories(path);
                    foreach (var s in subfolders)
                    {
                        CopyDirectory(s, src, dest, trusrc, trudest, recursive);
                    }
                }

                // Get all files of the folder
                var files = Directory.GetFiles(path);
                foreach (var f in files)
                {
                    relative = GetRelativePath(src.location, f);
                    if (SQLiteHandler.Exists(trusrc.location, trudest.location, relative))
                    {
                        if (File.GetLastWriteTime(f) <= File.GetLastWriteTime(Path.Combine(dest.location, relative))) continue;
                    } else
                    {
                        SQLiteHandler.Record(trusrc.location, trudest.location, relative);
                    }

                    // Copy the file
                    var target = Path.Combine(dest.location, relative);;
                    if (target.EndsWith(Path.DirectorySeparatorChar.ToString()))
                        target = target.Substring(0, target.Length - 1);
                    System.IO.File.Copy(f, target, true);
                    Console.WriteLine("Record file : " + f);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace, ex);
                //Thread.Sleep(_secInterval);
            }
        }
    }
}