﻿using System;
using System.Net;
using System.Text;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace EraseConsole
{
    public class Program
    {
        private static HttpServer wssv;

        public static void Main(string[] args)
        {
            wssv = new HttpServer(4649);
            wssv.RootPath = "./Files";
            wssv.AddWebSocketService<WSHandler>("/WSHandler");
            wssv.OnGet += Wssv_OnGet;
            wssv.Start();
            if (wssv.IsListening)
            {
                Console.WriteLine("Listening on port {0}, and providing WebSocket services:", wssv.Port);
                foreach (var path in wssv.WebSocketServices.Paths)
                    Console.WriteLine("- {0}", path);
            }

            Console.WriteLine("\nPress Enter key to stop the server...");
            Console.ReadLine();
            wssv.Stop();
        }

        private static void Wssv_OnGet(object sender, HttpRequestEventArgs e)
        {
            var req = e.Request;
            var res = e.Response;

            var path = req.RawUrl;
            if (path == "/")
                path += "Index.html";

            var content = wssv.GetFile(path);
            if (content == null)
            {
                res.StatusCode = (int)HttpStatusCode.NotFound;
                return;
            }

            if (path.EndsWith(".html"))
            {
                res.ContentType = "text/html";
                res.ContentEncoding = Encoding.UTF8;
            }
            else if (path.EndsWith(".js"))
            {
                res.ContentType = "application/javascript";
                res.ContentEncoding = Encoding.UTF8;
            }

            res.WriteContent(content);
        }
    }
}
