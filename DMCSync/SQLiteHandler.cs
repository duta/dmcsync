﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EraseConsole
{
    public static class SQLiteHandler
    {
        private static DbProviderFactory fact;

        public static List<Folders> GetAllDelete()
        {
            List<Folders> result = new List<Folders>();
            try {
                fact = DbProviderFactories.GetFactory("System.Data.SQLite");
                using (DbConnection cnn = fact.CreateConnection())
                {
                    cnn.ConnectionString = "Data Source=datafile.db";
                    cnn.Open();
                    var com = cnn.CreateCommand();
                    com.CommandText = "select location, servername, username, password from delfolder";
                    var reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        var f = new Folders();
                        f.location = reader.GetString(0);
                        if (!reader.IsDBNull(1))
                        {
                            f.server = reader.GetString(1);
                            f.user = reader.GetString(2);
                            f.pass = reader.GetString(3);
                        }
                        result.Add(f);
                    }
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        public static int GetDeleteDays()
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "select count from setting where name = 'deldays'";
                return (int)(long)com.ExecuteScalar();
            }
        }

        public static int GetDeleteInterval()
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "select count from setting where name = 'delinterval'";
                return (int)(long)com.ExecuteScalar();
            }
        }

        internal static void DeleteDelete(string v)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "delete from delfolder where location=?";
                com.Parameters.Add(new SQLiteParameter("location", v));
                com.ExecuteNonQuery();
            }
        }

        internal static void AddDelete(string v1, string v2, string v3, string v4)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "insert into delfolder (location, servername, username, password) VALUES (?,?,?,?)";
                com.Parameters.Add(new SQLiteParameter("location", v1));
                if (v2.Length == 0)
                {
                    com.Parameters.Add(new SQLiteParameter("servername", null));
                    com.Parameters.Add(new SQLiteParameter("username", null));
                    com.Parameters.Add(new SQLiteParameter("password", null));
                }
                else
                {
                    com.Parameters.Add(new SQLiteParameter("servername", v2));
                    com.Parameters.Add(new SQLiteParameter("username", v3));
                    com.Parameters.Add(new SQLiteParameter("password", v4));
                }
                com.ExecuteNonQuery();
            }
        }

        public static void ChangeSettings(int v1, int v2)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "update setting set count = " + v1 + " where name = 'deldays'";
                com.ExecuteNonQuery();
                com.CommandText = "update setting set count = " + v2 + " where name = 'delinterval'";
                com.ExecuteNonQuery();
            }
        }

        public static Settings GetSettings()
        {
            var s = new Settings();
            s.deldays = GetDeleteDays();
            s.delinterval = GetDeleteInterval();
            return s;
        }

        public static List<Folders[]> GetAllSyncOne()
        {
            List<Folders[]> result = new List<Folders[]>();
            try
            {
                fact = DbProviderFactories.GetFactory("System.Data.SQLite");
                using (DbConnection cnn = fact.CreateConnection())
                {
                    cnn.ConnectionString = "Data Source=datafile.db";
                    cnn.Open();
                    var com = cnn.CreateCommand();
                    com.CommandText = "select srclocation, srcservername, srcusername, srcpassword, destlocation, destservername, destusername, destpassword from synconefolder";
                    var reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        var f = new Folders();
                        var f2 = new Folders();
                        f.location = reader.GetString(0);
                        if (!reader.IsDBNull(1))
                        {
                            f.server = reader.GetString(1);
                            f.user = reader.GetString(2);
                            f.pass = reader.GetString(3);
                        }
                        f2.location = reader.GetString(4);
                        if (!reader.IsDBNull(5))
                        {
                            f2.server = reader.GetString(5);
                            f2.user = reader.GetString(6);
                            f2.pass = reader.GetString(7);
                        }
                        result.Add(new Folders[] { f, f2 });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        internal static void DeleteSyncOne(string v, string v2)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "delete from synconefolder where srclocation=? and destlocation=?";
                com.Parameters.Add(new SQLiteParameter("srclocation", v));
                com.Parameters.Add(new SQLiteParameter("destlocation", v2));
                com.ExecuteNonQuery();
            }
        }

        internal static void AddSyncOne(string v1, string v2, string v3, string v4, string v11, string v12, string v13, string v14)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "insert into synconefolder (srclocation, srcservername, srcusername, srcpassword, destlocation, destservername, destusername, destpassword) VALUES (?,?,?,?,?,?,?,?)";
                com.Parameters.Add(new SQLiteParameter("srclocation", v1));
                if (v2.Length == 0)
                {
                    com.Parameters.Add(new SQLiteParameter("srcservername", null));
                    com.Parameters.Add(new SQLiteParameter("srcusername", null));
                    com.Parameters.Add(new SQLiteParameter("srcpassword", null));
                }
                else
                {
                    com.Parameters.Add(new SQLiteParameter("srcservername", v2));
                    com.Parameters.Add(new SQLiteParameter("srcusername", v3));
                    com.Parameters.Add(new SQLiteParameter("srcpassword", v4));
                }
                com.Parameters.Add(new SQLiteParameter("destlocation", v11));
                if (v2.Length == 0)
                {
                    com.Parameters.Add(new SQLiteParameter("destservername", null));
                    com.Parameters.Add(new SQLiteParameter("destusername", null));
                    com.Parameters.Add(new SQLiteParameter("destpassword", null));
                }
                else
                {
                    com.Parameters.Add(new SQLiteParameter("destservername", v12));
                    com.Parameters.Add(new SQLiteParameter("destusername", v13));
                    com.Parameters.Add(new SQLiteParameter("destpassword", v14));
                }
                com.ExecuteNonQuery();
            }
        }

        internal static void Erase(string location1, string location2, string relative)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "delete from synctwohistory where srclocation=? and destlocation=? and relatives=?";
                com.Parameters.Add(new SQLiteParameter("srclocation", location1));
                com.Parameters.Add(new SQLiteParameter("destlocation", location2));
                com.Parameters.Add(new SQLiteParameter("relatives", relative));
                com.ExecuteNonQuery();
            }
        }

        internal static void Record(string location1, string location2, string relative)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "insert into synctwohistory (srclocation, destlocation, relatives) values (?,?,?)";
                com.Parameters.Add(new SQLiteParameter("srclocation", location1));
                com.Parameters.Add(new SQLiteParameter("destlocation", location2));
                com.Parameters.Add(new SQLiteParameter("relatives", relative));
                com.ExecuteNonQuery();
            }
        }

        internal static bool Exists(string location1, string location2, string relative)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "select * from synctwohistory where srclocation=? and destlocation=? and relatives=?";
                com.Parameters.Add(new SQLiteParameter("srclocation", location1));
                com.Parameters.Add(new SQLiteParameter("destlocation", location2));
                com.Parameters.Add(new SQLiteParameter("relatives", relative));
                var red = com.ExecuteReader();
                var ret = red.HasRows;
                red.Close();
                return ret;
            }
        }

        internal static List<string> GetS2History(string location1, string location2)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                List<string> res = new List<string>();
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "select relatives from synctwohistory where srclocation=? and destlocation=? ORDER BY relatives desc";
                com.Parameters.Add(new SQLiteParameter("srclocation", location1));
                com.Parameters.Add(new SQLiteParameter("destlocation", location2));
                var red = com.ExecuteReader();
                while (red.Read())
                {
                    res.Add(red.GetString(0));
                }
                return res;
            }
        }

        public static List<Folders[]> GetAllSyncTwo()
        {
            List<Folders[]> result = new List<Folders[]>();
            try
            {
                fact = DbProviderFactories.GetFactory("System.Data.SQLite");
                using (DbConnection cnn = fact.CreateConnection())
                {
                    cnn.ConnectionString = "Data Source=datafile.db";
                    cnn.Open();
                    var com = cnn.CreateCommand();
                    com.CommandText = "select srclocation, srcservername, srcusername, srcpassword, destlocation, destservername, destusername, destpassword from synctwofolder";
                    var reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        var f = new Folders();
                        var f2 = new Folders();
                        f.location = reader.GetString(0);
                        if (!reader.IsDBNull(1))
                        {
                            f.server = reader.GetString(1);
                            f.user = reader.GetString(2);
                            f.pass = reader.GetString(3);
                        }
                        f2.location = reader.GetString(4);
                        if (!reader.IsDBNull(5))
                        {
                            f2.server = reader.GetString(5);
                            f2.user = reader.GetString(6);
                            f2.pass = reader.GetString(7);
                        }
                        result.Add(new Folders[] { f, f2 });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        internal static void DeleteSyncTwo(string v, string v2)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "delete from synctwofolder where srclocation=? and destlocation=?";
                com.Parameters.Add(new SQLiteParameter("srclocation", v));
                com.Parameters.Add(new SQLiteParameter("destlocation", v2));
                com.ExecuteNonQuery();
            }
        }

        internal static void AddSyncTwo(string v1, string v2, string v3, string v4, string v11, string v12, string v13, string v14)
        {
            fact = DbProviderFactories.GetFactory("System.Data.SQLite");
            using (DbConnection cnn = fact.CreateConnection())
            {
                cnn.ConnectionString = "Data Source=datafile.db";
                cnn.Open();
                var com = cnn.CreateCommand();
                com.CommandText = "insert into synctwofolder (srclocation, srcservername, srcusername, srcpassword, destlocation, destservername, destusername, destpassword) VALUES (?,?,?,?,?,?,?,?)";
                com.Parameters.Add(new SQLiteParameter("srclocation", v1));
                if (v2.Length == 0)
                {
                    com.Parameters.Add(new SQLiteParameter("srcservername", null));
                    com.Parameters.Add(new SQLiteParameter("srcusername", null));
                    com.Parameters.Add(new SQLiteParameter("srcpassword", null));
                }
                else
                {
                    com.Parameters.Add(new SQLiteParameter("srcservername", v2));
                    com.Parameters.Add(new SQLiteParameter("srcusername", v3));
                    com.Parameters.Add(new SQLiteParameter("srcpassword", v4));
                }
                com.Parameters.Add(new SQLiteParameter("destlocation", v11));
                if (v2.Length == 0)
                {
                    com.Parameters.Add(new SQLiteParameter("destservername", null));
                    com.Parameters.Add(new SQLiteParameter("destusername", null));
                    com.Parameters.Add(new SQLiteParameter("destpassword", null));
                }
                else
                {
                    com.Parameters.Add(new SQLiteParameter("destservername", v12));
                    com.Parameters.Add(new SQLiteParameter("destusername", v13));
                    com.Parameters.Add(new SQLiteParameter("destpassword", v14));
                }
                com.ExecuteNonQuery();
            }
        }
    }

    public class Settings
    {
        public string Type = "setting";
        public int deldays;
        public int delinterval;
    }

    public class Folders
    {
        public string location;
        public string server;
        public string user;
        public string pass;
    }
}
